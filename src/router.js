import React from 'react';
import {useColorScheme} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {themes} from './utils/themes';
import {route} from './utils/routes';

const Stack = createNativeStackNavigator();

function Route() {
  const scheme = useColorScheme();
  return (
    <SafeAreaProvider>
      <NavigationContainer
        theme={scheme === 'dark' ? themes.dark : themes.default}>
        <Stack.Navigator initialRouteName="splash">
          {route.map(item => (
            <Stack.Screen
              key={item.key}
              name={item.name}
              component={item.component}
              options={item.options}
            />
          ))}
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

export default Route;
