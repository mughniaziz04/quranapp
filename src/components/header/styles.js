import {StyleSheet} from 'react-native';

export const useStyles = colors =>
  StyleSheet.create({
    header: {
      flexDirection: 'row',
      // justifyContent: 'space-between',
      height: 50,
      alignItems: 'center',
      paddingHorizontal: 20,
    },
    viewMenu: {
      width: '15%',
      height: '100%',
      alignItems: 'flex-start',
      justifyContent: 'center',
    },
    viewTitle: {
      width: '70%',
      height: '100%',
      justifyContent: 'center',
    },
    viewSearch: {
      width: '15%',
      height: '100%',
      alignItems: 'flex-end',
      justifyContent: 'center',
    },
    title: {
      fontSize: 18,
      color: colors.textPrimary,
      fontWeight: '700',
    },
    inputText: {
      borderBottomWidth: 1,
      borderBottomColor: colors.border,
      fontFamily: 'Poppins-Regular',
      fontSize: 14,
      color: colors.textLatin,
      width: '80%',
      paddingBottom: 0,
    },
    close: {
      fontSize: 26,
      color: colors.textLatin,
      fontFamily: 'Poppins-Bold',
    },
    btnSearch: {
      width: '90%',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: 5,
    },
  });
