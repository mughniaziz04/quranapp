import React from 'react';
import {View, TouchableOpacity, Text, TextInput} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Menu, Search, IconBack} from '../../assets';
import {useStyles} from './styles';

function Header({
  title,
  canBack,
  onPress,
  isEdit,
  onChange,
  onPressEdit,
  search,
}) {
  const {colors} = useTheme();
  const styles = useStyles(colors);
  return (
    <View style={styles.header}>
      <View style={styles.viewMenu}>
        <TouchableOpacity
          onPress={canBack ? onPress : () => console.log('pressed')}>
          {canBack ? <IconBack /> : <Menu />}
        </TouchableOpacity>
      </View>
      {isEdit ? (
        <TouchableOpacity style={styles.btnSearch} onPress={onPressEdit}>
          <TextInput
            style={styles.inputText}
            onChangeText={onChange}
            onSubmitEditing={search}
          />
          <Text style={styles.close}>X</Text>
        </TouchableOpacity>
      ) : (
        <>
          <View style={styles.viewTitle}>
            <Text style={styles.title}>{title}</Text>
          </View>
          <View style={styles.viewSearch}>
            <TouchableOpacity onPress={onPressEdit}>
              <Search />
            </TouchableOpacity>
          </View>
        </>
      )}
    </View>
  );
}

export default Header;
