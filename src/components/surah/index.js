import React from 'react';
import {View, Text, ImageBackground, TouchableOpacity} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {useStyles} from './styles';
import {Position} from '../../assets';

function SurahItem({item, onPress}) {
  const {colors} = useTheme();
  const styles = useStyles(colors);
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.viewContent}>
        <View style={styles.viewLeft}>
          <ImageBackground source={Position} style={styles.imgBg}>
            <Text style={styles.fontPosition}>{item?.item.number}</Text>
          </ImageBackground>
          <View style={styles.viewName}>
            <Text style={styles.textName}>{item?.item.englishName}</Text>
            <View style={styles.viewDesc}>
              <Text style={styles.textDesc}>
                {item?.item.revelationType?.toUpperCase()}
              </Text>
              <View style={styles.dot} />
              <Text style={styles.textDesc}>
                {item?.item.numberOfAyahs} VERSES
              </Text>
            </View>
          </View>
        </View>
        <View>
          <Text style={styles.textArab}>{item?.item.name}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default SurahItem;
