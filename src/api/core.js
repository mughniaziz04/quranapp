import {createAction} from '@reduxjs/toolkit';

export const apiGetPlace = createAction('core/getPlace');
