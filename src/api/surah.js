import {createAction} from '@reduxjs/toolkit';

export const apiGetSurah = createAction('surah/getSurah', {});
export const apiGetSurahSuccess = createAction('surah/getSurahSuccess', {});
export const apiGetSurahFailed = createAction('surah/getSurahFailed', {});
