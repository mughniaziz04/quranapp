import {createAction} from '@reduxjs/toolkit';

export const apiGetTimming = createAction('prayer/getTimming');
export const apiGetTimmingSuccess = createAction('prayer/getTimmingSuccess');
export const apiGetTimmingFailure = createAction('prayer/getTimmingFailure');
