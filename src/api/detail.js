import {createAction} from '@reduxjs/toolkit';

export const apiGetDetail = createAction('detail/getDetailSurah');
export const apiGetDetailSurahSuccess = createAction(
  'detail/getDetailSurahSuccess',
);
export const apiGetDetailSurahFailure = createAction(
  'detail/getDetailSurahFailure',
);
export const apiUpdateOffsets = createAction('detail/updateOffset');
