import {createAction} from '@reduxjs/toolkit';

export const apiCallMeta = createAction('home/getMeta');
export const apiCallMetaSuccess = createAction('home/getMetaSuccess');
export const apiCallMetaFailure = createAction('home/getMetaFailure');
