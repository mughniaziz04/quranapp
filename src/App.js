import React, {useEffect} from 'react';
import {PermissionsAndroid, Platform} from 'react-native';
import {useDispatch} from 'react-redux';
import {setPermission} from './redux/actions/core';
import Route from './router';

function App() {
  const dispatch = useDispatch();
  const requestPermission = async () => {
    if (Platform.OS === 'android') {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION ||
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      );
      if (granted === 'granted') {
        dispatch(setPermission(true));
      }
    }
  };
  useEffect(() => {
    requestPermission();
  });

  return <Route key="router" />;
}

export default App;
