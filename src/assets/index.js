import BgQuranApp from './images/BgQuranApp.svg';
import PrayActive from './icons/PrayActive.svg';
import PrayInactive from './icons/PrayInactive.svg';
import ReadActive from './icons/ReadActive.svg';
import ReadInactive from './icons/ReadInactive.svg';
import Menu from './icons/Menu.svg';
import Search from './icons/Search.svg';
import LastReadBg from './images/LastReadBg.png';
import LastRead from './icons/LastRead.svg';
import Position from './images/Position.png';
import IconBack from './icons/IconBack.svg';
import BgSurah from './images/BgSurah.png';

export {
  BgQuranApp,
  PrayActive,
  PrayInactive,
  ReadActive,
  ReadInactive,
  Menu,
  Search,
  LastReadBg,
  LastRead,
  Position,
  IconBack,
  BgSurah,
};
