import axios from 'axios';
import * as actions from '../api/prayer';

const prayerApi =
  ({dispatch}) =>
  next =>
  async action => {
    if (action.type !== actions.apiGetTimming.type) {
      return next(action);
    }

    const {onSuccess, onFailure, onStart, address} = action.payload;

    if (onStart) {
      dispatch({type: onStart});
    }
    next(action);

    await axios
      .request({
        method: 'GET',
        url: 'http://api.aladhan.com/v1/calendarByAddress',
        headers: {
          'Content-Type': 'application/json',
        },
        params: {
          address: address,
        },
      })
      .then(res => {
        return dispatch({type: onSuccess, payload: res?.data?.data});
      })
      .catch(err => {
        return dispatch({type: onFailure, payload: err.response});
      });
  };

export default prayerApi;
