import axios from 'axios';
import * as actions from '../api/detail';

const detailApi =
  ({dispatch}) =>
  next =>
  async action => {
    if (action.type !== actions.apiGetDetail.type) {
      return next(action);
    }

    // console.log(action.payload);
    const {onSuccess, onError, onStarts, urls, id, offsets, limit} =
      action?.payload;

    if (onStarts) {
      dispatch({type: onStarts});
    }
    next(action);

    await axios
      .request({
        baseURL: 'http://api.alquran.cloud/v1',
        url: urls.replace(':id', id),
        headers: {
          'Content-Type': 'application/json',
        },
        params: {
          limit,
          offset: offsets,
        },
      })
      .then(res => {
        // console.log(res.data);
        return dispatch({type: onSuccess, payload: res.data.data});
        // dispatch({type: onUpdate, payload: res.data.data[0].length});
      })
      .catch(err => {
        console.log('err', err);
        return dispatch({type: onError, payload: err.response});
      });
  };

export default detailApi;
