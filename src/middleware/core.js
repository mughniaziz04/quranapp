import axios from 'axios';
import * as actions from '../api/core';

const coreApi =
  ({dispatch}) =>
  next =>
  async action => {
    if (action.type !== actions.apiGetPlace.type) {
      return next(action);
    }

    console.log('ACTION -> ', action.payload);

    const {params, onSuccess} = action.payload;
    next(action);

    await axios
      .request({
        method: 'GET',
        url: 'https://maps.googleapis.com/maps/api/geocode/json',
        headers: {
          'Content-Type': 'application/json',
        },
        params: {
          latlng: params?.lat + ',' + params?.long,
          sensor: params?.sensor,
          key: params?.key,
        },
      })
      .then(res => {
        return dispatch({
          type: onSuccess,
          payload: res?.data?.results[0]?.formatted_address,
        });
      })
      .catch(err => console.log('ERR -> ', err));
  };

export default coreApi;
