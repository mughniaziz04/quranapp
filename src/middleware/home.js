import axios from 'axios';
import * as actions from '../api/home';

const homeApi =
  ({dispatch}) =>
  next =>
  async action => {
    if (action.type !== actions.apiCallMeta.type) {
      return next(action);
    }

    const {onStart, onSuccess, onError} = action.payload;

    if (onStart) {
      dispatch({type: onStart});
    }
    next(action);

    await axios({
      method: 'GET',
      url: 'http://api.alquran.cloud/v1/meta',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(res => {
        return dispatch({type: onSuccess, payload: res.data.data});
      })
      .catch(error => {
        return dispatch({type: onError, payload: error.response});
      });
  };

export default homeApi;
