import axios from 'axios';
import * as actions from '../api/surah';
import {url} from '../redux/surah';

const surahApi =
  ({dispatch}) =>
  next =>
  async action => {
    if (action.type !== actions.apiGetSurah.type) {
      return next(action);
    }
    const {onSuccess, onError} = action.payload;

    // if (onStart) {
    //   dispatch({type: onStart});
    // }
    // next(action);

    try {
      const response = await axios({
        method: 'GET',
        url: url,
        headers: {
          'Content-Type': 'application/json',
        },
      });
      if (onSuccess) {
        dispatch({type: onSuccess, payload: response.data?.data?.surahs});
      }
    } catch (error) {
      if (onError) {
        dispatch({type: onError, payload: error.response});
      }
    }
  };

export default surahApi;
