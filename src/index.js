import React from 'react';
import {Provider} from 'react-redux';
import {configureStore} from '@reduxjs/toolkit';
import {persistReducer, persistStore} from 'redux-persist';
import {PersistGate} from 'redux-persist/integration/react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import rootReducers from './reducers';
import thunk from 'redux-thunk';
import App from './App';
import surahApi from './middleware/surah';
import detailApi from './middleware/detail';
import homeApi from './middleware/home';
import coreApi from './middleware/core';
import prayerApi from './middleware/prayer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['core'],
};

const persistedReducers = persistReducer(persistConfig, rootReducers);

const store = configureStore({
  reducer: persistedReducers,
  middleware: [thunk],
});

const persistor = persistStore(store);

function Root() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  );
}

export default Root;
