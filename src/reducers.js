import {combineReducers} from 'redux';
import coreReducer from './redux/reducers/core';
import homeReducer from './redux/reducers/home';
import detailReducer from './redux/reducers/detail';

const rootReducers = combineReducers({
  core: coreReducer,
  home: homeReducer,
  detail: detailReducer,
});

export default rootReducers;
