export const palette = {
  default: {
    background: '#FFFFFF',
    textPrimary: '#672CBC',
    text: '#F0F',
    textSecondary: '#8789A3',
    textLatin: '#240F4F',
    textWhite: '#FFF',
    btnSecondary: '#F9B091',
    border: 'rgba(217, 217, 217, 1)',
    cardHeader: 'rgba(18,25, 49, 0.05)',
    roundAyat: '#863ED5',
  },
};
