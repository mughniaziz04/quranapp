import DetailSurah from '../screens/detail';
import Home from '../screens/home';
import Main from '../screens/main';
import Prayer from '../screens/prayer';
import Splash from '../screens/splash';
import Surah from '../screens/surah';
import Walkthrough from '../screens/walkthrough';

export const route = [
  {
    key: 'splash',
    name: 'splash',
    component: Splash,
    options: {headerShown: false},
  },
  {
    key: 'walk',
    name: 'walk',
    component: Walkthrough,
    options: {headerShown: false},
  },
  {
    key: 'home',
    name: 'home',
    component: Home,
    options: {headerShown: false},
  },
  {
    key: 'main',
    name: 'main',
    component: Main,
    options: {headerShown: false},
  },
  {
    key: 'pray',
    name: 'pray',
    component: Prayer,
    options: {headerShown: false},
  },
  {
    key: 'surah',
    name: 'surah',
    component: Surah,
    options: {headerShown: false},
  },
  {
    key: 'detail',
    name: 'detail',
    component: DetailSurah,
    options: {headerShown: false},
  },
];
