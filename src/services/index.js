import * as SPLASH from './splash';
import * as HOME from './home';
import * as DETAIL from './detail';

export {SPLASH, HOME, DETAIL};
