import axios from 'axios';

export const getPlaceName = params => {
  return axios.request({
    method: 'GET',
    url: 'https://maps.googleapis.com/maps/api/geocode/json',
    headers: {
      'Content-Type': 'application/json',
    },
    params: {
      latlng: params?.lat + ',' + params?.long,
      sensor: params?.sensor,
      key: params?.key,
    },
  });
};
