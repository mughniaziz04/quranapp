import axios from 'axios';

export const getMetaData = () => {
  return axios.request({
    method: 'GET',
    url: 'http://api.alquran.cloud/v1/meta',
    headers: {
      'Content-Type': 'application/json',
    },
  });
};
