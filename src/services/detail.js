import axios from 'axios';

export const detailSurah = (id, offset, limit) => {
  return axios.request({
    method: 'GET',
    baseURL: 'http://api.alquran.cloud/v1',
    url: '/surah/:id/editions/quran-uthmani,en.asad'.replace(':id', id),
    params: {
      limit,
      offset,
    },
  });
};
