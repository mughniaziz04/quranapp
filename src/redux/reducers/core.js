import * as CONST from '../constants/core';

const initialState = {
  walkthrough: false,
  permissionGrant: false,
  position: {},
  placeName: '',
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case CONST.SET_WALKTHROUGH:
      return {
        ...state,
        walkthrough: action.payload,
      };
    case CONST.SET_GRANT:
      return {
        ...state,
        permissionGrant: action.payload,
      };
    case CONST.SET_POSITION:
      return {
        ...state,
        position: action.payload,
      };
    case CONST.GET_PLACENAME:
      return {
        ...state,
        placeName: action.payload,
      };
    default:
      return state;
  }
};

export default reducers;
