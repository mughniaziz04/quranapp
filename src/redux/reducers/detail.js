import * as CONST from '../constants/detail';

const initialState = {
  isLoading: false,
  arab: [],
  trans: [],
  offset: 0,
  limit: 20,
  message: '',
  error: null,
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case CONST.GET_DETAIL_SURAH:
      return {
        ...state,
        isLoading: true,
      };
    case CONST.GET_DETAIL_SURAH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        arab: {
          ...state.arab,
          ayahs: [
            ...(state?.arab?.ayahs ? state.arab.ayahs : []),
            ...action.payload[0]?.ayahs,
          ],
        },
        trans: {
          ...state.trans,
          ayahs: [
            ...(state?.trans?.ayahs ? state.trans.ayahs : []),
            ...action.payload[1]?.ayahs,
          ],
        },
      };
    case CONST.GET_DETAIL_SURAH_FAILURE:
      return {
        ...state,
        isLoading: false,
        message: 'Error while get detail',
        error: action.payload,
      };
    case CONST.RESET:
      return {
        ...state,
        arab: [],
        trans: [],
        offset: 0,
      };
    default:
      return state;
  }
};

export default reducers;
