import * as CONST from '../constants/home';

const initialState = {
  isLoading: false,
  data: [],
  message: '',
  error: null,
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case CONST.GET_META_DATA:
      return {
        ...state,
        isLoading: true,
      };
    case CONST.GET_META_DATA_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    case CONST.GET_META_DATA_FAILURE:
      return {
        ...state,
        isLoading: false,
        message: 'Error get meta data',
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducers;
