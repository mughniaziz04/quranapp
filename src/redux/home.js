import {createSlice} from '@reduxjs/toolkit';
import {apiCallMeta} from '../api/home';

const homeSlice = createSlice({
  name: 'home',
  initialState: {
    isLoad: false,
    data: [],
    message: '',
    error: null,
  },
  reducers: {
    getMeta: (home, action) => {
      console.log('home -> ', home);
      home.isLoad = true;
    },
    getMetaSuccess: (home, action) => {
      home.isLoad = false;
      home.data = action.payload;
    },
    getMetaFailure: (home, action) => {
      home.isLoad = false;
      home.message = 'Get Meta Data Failed';
      home.error = action.payload;
    },
  },
});

export const {getMeta, getMetaSuccess, getMetaFailure} = homeSlice.actions;

export default homeSlice;

export const getDataMeta = () => dispatch => {
  return dispatch(
    apiCallMeta({
      onStart: getMeta.type,
      onSuccess: getMetaSuccess.type,
      onError: getMetaFailure.type,
    }),
  );
};
