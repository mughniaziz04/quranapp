import {createSlice} from '@reduxjs/toolkit';
import {apiGetTimming} from '../api/prayer';

const prayerSlice = createSlice({
  name: 'prayer',
  initialState: {
    isLoad: false,
    timing: [],
    message: '',
  },
  reducers: {
    getTimming: (prayer, action) => {
      prayer.isLoad = true;
    },
    getTimmingSuccess: (prayer, action) => {
      prayer.isLoad = false;
      prayer.timing = action.payload;
    },
    getTimmingFailure: (prayer, action) => {
      prayer.isLoad = false;
      prayer.message = 'Failed get Timming';
    },
  },
});

export const {getTimming, getTimmingSuccess, getTimmingFailure} =
  prayerSlice.actions;

export default prayerSlice;

export const getDataTimming = address => dispatch => {
  dispatch(
    apiGetTimming({
      address,
      onSuccess: getTimmingSuccess.type,
      onFailure: getTimmingFailure.type,
      onStart: getTimming.type,
    }),
  );
};
