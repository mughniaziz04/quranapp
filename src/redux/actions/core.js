import * as CONST from '../constants/core';

export const setWalkthrough = payload => async dispatch => {
  dispatch(dispatcher.setWalk(payload));
};

export const setPermission = payload => async dispatch => {
  dispatch(dispatcher.setGrant(payload));
};

export const setPosition = payload => async dispatch => {
  dispatch(dispatcher.setPosition(payload));
};

const dispatcher = {
  setWalk: payload => ({
    type: CONST.SET_WALKTHROUGH,
    payload,
  }),
  setGrant: payload => ({
    type: CONST.SET_GRANT,
    payload,
  }),
  setPosition: payload => ({
    type: CONST.SET_POSITION,
    payload,
  }),
};
