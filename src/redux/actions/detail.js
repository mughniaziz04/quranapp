import * as CONST from '../constants/detail';
import {DETAIL} from '../../services';

export const getDetailSurah = (id, offset, limit) => async dispatch => {
  dispatch(dispatcher.getDetail());
  await DETAIL.detailSurah(id, offset, limit)
    .then(res => {
      console.log('RESPONSE -> ', res);
      if (res.status === 200) {
        dispatch(dispatcher.getDetailSuccess(res.data.data));
      }
    })
    .catch(err => dispatch(dispatcher.getDetailFailure(err)));
};

export const resetState = () => async dispatch => {
  dispatch(dispatcher.reset());
};

const dispatcher = {
  getDetail: () => ({
    type: CONST.GET_DETAIL_SURAH,
  }),
  getDetailSuccess: payload => ({
    type: CONST.GET_DETAIL_SURAH_SUCCESS,
    payload,
  }),
  getDetailFailure: payload => ({
    type: CONST.GET_DETAIL_SURAH_FAILURE,
    payload,
  }),
  reset: () => ({
    type: CONST.RESET,
  }),
};
