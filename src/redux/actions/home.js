import * as CONST from '../constants/home';
import {HOME} from '../../services';

export const getMeta = () => async dispatch => {
  dispatch(dispatcher.metaLoad());
  await HOME.getMetaData()
    .then(res => {
      if (res.status === 200) {
        dispatch(dispatcher.metaSuccess(res.data.data));
      }
    })
    .catch(err => dispatch(dispatcher.metaFailure(err)));
};

const dispatcher = {
  metaLoad: () => ({
    type: CONST.GET_META_DATA,
  }),
  metaSuccess: payload => ({
    type: CONST.GET_META_DATA_SUCCESS,
    payload,
  }),
  metaFailure: payload => ({
    type: CONST.GET_META_DATA_FAILURE,
    payload,
  }),
};
