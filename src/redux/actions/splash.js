import * as CONST from '../constants/splash';
import {SPLASH} from '../../services';

export const getPlace = params => async dispatch => {
  dispatch(dispatcher.placeLoad());
  await SPLASH.getPlaceName(params)
    .then(res =>
      dispatch(dispatcher.placeSuccess(res.data.results[0].formatted_address)),
    )
    .catch(err => console.log('ERR -> ', err));
};

const dispatcher = {
  placeLoad: () => ({
    type: CONST.GET_PLACENAME,
  }),
  placeSuccess: payload => ({
    type: CONST.GET_PLACENAME_SUCCESS,
    payload,
  }),
};
