import {createSlice} from '@reduxjs/toolkit';
import {apiGetDetail, apiUpdateOffsets} from '../api/detail';

const detailSlice = createSlice({
  name: 'detail',
  initialState: {
    isLoad: false,
    arab: [],
    trans: [],
    offset: 0,
    limit: 20,
    message: '',
  },
  reducers: {
    getDetailSurah: (detail, action) => {
      detail.isLoad = true;
    },
    getDetailSurahSuccess: (detail, action) => {
      // console.log('arab', action?.payload[0]?.ayahs?.length);
      // console.log('trans', detail?.arab?.ayahs?.length);
      detail.isLoad = false;
      detail.arab = {
        ...detail.arab,
        ayahs: [
          ...(detail?.arab?.ayahs ? detail.arab.ayahs : []),
          ...action.payload[0]?.ayahs,
        ],
      };
      detail.trans = {
        ...detail.trans,
        ayahs: [
          ...(detail?.trans?.ayahs ? detail.trans.ayahs : []),
          ...action.payload[1]?.ayahs,
        ],
      };
      // detail.offset = detail?.arab?.ayahs?.length || 0;
    },
    getDetailSurahFailure: (detail, action) => {
      detail.isLoad = false;
      detail.message = 'Surah cannot load';
    },
    updateOffsets: (detail, action) => {
      detail.offset = detail?.arab?.ayahs?.length;
    },
    resetState: detail => {
      detail.offset = 0;
      detail.arab = [];
      detail.trans = [];
    },
  },
});

export const {
  getDetailSurah,
  getDetailSurahSuccess,
  getDetailSurahFailure,
  updateOffsets,
  resetState,
} = detailSlice.actions;

export default detailSlice;

const urls = '/surah/:id/editions/quran-uthmani,en.asad';

export const getDataDetail = (id, offsets) => dispatch => {
  return dispatch(
    apiGetDetail({
      urls,
      id,
      offsets,
      onSuccess: getDetailSurahSuccess.type,
      onError: getDetailSurahFailure.type,
      onStarts: getDetailSurah.type,
    }),
  );
};

export const updateDataOffset = count => dispatch => {
  return dispatch(
    apiUpdateOffsets({
      count,
      onUpdate: updateOffsets.type,
    }),
  );
};
