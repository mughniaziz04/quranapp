import {createSlice} from '@reduxjs/toolkit';
import {apiGetSurah} from '../api/surah';

const surahSlice = createSlice({
  name: 'surah',
  initialState: {
    isLoad: false,
    message: '',
    surah: [],
  },
  reducers: {
    getSurah: (surah, action) => {
      surah.isLoad = true;
    },
    getSurahSuccess: (surah, action) => {
      surah.isLoad = false;
      surah.surah = action.payload;
    },
    getSurahFailed: (surah, action) => {
      surah.isLoad = false;
      surah.message = 'Failed to get surah';
    },
  },
});

export const {getSurah, getSurahSuccess, getSurahFailed} = surahSlice.actions;

export default surahSlice;

export const url = 'https://api.alquran.cloud/v1/quran/quran-uthmani';

export const getDataSurah = () => async dispatch => {
  return await dispatch(
    apiGetSurah({
      onStart: getSurah.type,
      onSuccess: getSurahSuccess.type,
      onError: getSurahFailed.type,
    }),
  );
};
