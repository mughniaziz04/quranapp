import {StyleSheet} from 'react-native';

export const useStyles = colors =>
  StyleSheet.create({
    safeArea: {
      flex: 1,
      backgroundColor: colors.background,
    },
    container: {
      padding: 20,
    },
    textWelcome: {
      fontSize: 16,
      color: colors.textSecondary,
      letterSpacing: 2,
      fontFamily: 'Poppins-Medium',
    },
    textName: {
      fontSize: 20,
      lineHeight: 24,
      color: colors.textLatin,
      marginTop: 8,
      fontFamily: 'Poppins-Bold',
    },
    imgBg: {
      padding: 20,
      overflow: 'hidden',
      marginTop: 40,
      height: 131,
    },
    viewLast: {
      flexDirection: 'row',
    },
    textLast: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      color: colors.textWhite,
      marginLeft: 5,
    },
    textSurah: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 16,
      color: colors.textWhite,
    },
    textAyah: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      color: colors.textWhite,
    },
    viewSurah: {
      marginTop: 20,
    },
    viewBar: {
      flexDirection: 'row',
      height: 50,
      width: '100%',
      justifyContent: 'space-between',
      marginTop: 20,
    },
    btnBar: {
      width: '50%',
      alignItems: 'center',
      justifyContent: 'center',
      borderBottomWidth: 2,
      borderBottomColor: colors.border,
    },
    btnBarActive: {
      borderBottomWidth: 3,
      borderBottomColor: colors.textPrimary,
    },
    textBar: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 16,
      color: colors.textSecondary,
    },
    textBarActive: {
      fontFamily: 'Poppins-Bold',
      fontSize: 16,
      color: colors.textPrimary,
    },
  });
