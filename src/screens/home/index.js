import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getMeta} from '../../redux/actions/home';
import {useTheme} from '@react-navigation/native';
import {useStyles} from './styles';
import {LastReadBg, LastRead} from '../../assets';
import {Header} from '../../components';
import Surah from '../surah';
import Juz from '../juz';

function Home({navigation}) {
  const {colors} = useTheme();
  const styles = useStyles(colors);
  const dispatch = useDispatch();
  const {data} = useSelector(state => state.home);

  const [surah, setSurah] = useState(true);
  const [juz, setJuz] = useState(false);
  const [lastRead] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [keyword, setKeyword] = useState('');
  const [filtered, setFiltered] = useState([]);

  useEffect(() => {
    dispatch(getMeta());
  }, [dispatch]);

  const handleBarMenu = type => {
    if (type === 'surah') {
      setSurah(true);
      setJuz(false);
    } else if (type === 'juz') {
      setSurah(false);
      setJuz(true);
    }
  };

  const searchSurah = () => {
    const resFilter = data?.surahs?.references?.filter(item => {
      return (
        item?.englishName.toLowerCase().search(keyword) > -1 ||
        item?.number.toString() === keyword ||
        item?.numberOfAyahs.toString() === keyword
      );
    });

    setFiltered(resFilter);
  };

  const searchJuz = () => {
    const resFilter = data?.surahs?.references?.filter(_surah => {
      // console.log('SURAH', _surah);
      return data?.juzs?.references?.find(_juz => {
        // console.log('JUZ', _juz);
        return _juz?.surah === _surah?.number;
      });
    });
    const filterRes = resFilter.filter(res => {
      return (
        res?.number.toString() === keyword ||
        res?.englishName.toLowerCase().search(keyword) > -1 ||
        res?.numberOfAyahs.toString() === keyword
      );
    });
    setFiltered(filterRes);
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <Header
        title="Qur'an App"
        isEdit={isEdit}
        onPressEdit={() => {
          setIsEdit(!isEdit);
          setFiltered([]);
        }}
        onChange={text => setKeyword(text)}
        search={() => (surah ? searchSurah() : searchJuz())}
      />
      <View style={styles.container}>
        <Text style={styles.textWelcome}>Assalamu'alaikum</Text>
        <Text style={styles.textName}>Tanvir Ahassan</Text>
        {lastRead ? (
          <ImageBackground
            source={LastReadBg}
            style={styles.imgBg}
            borderRadius={20}>
            <View>
              <View style={styles.viewLast}>
                <LastRead />
                <Text style={styles.textLast}>Last Read</Text>
              </View>
              <View style={styles.viewSurah}>
                <Text style={styles.textSurah}>Al-Faatiha</Text>
                <Text style={styles.textAyah}>Ayah No. 1</Text>
              </View>
            </View>
          </ImageBackground>
        ) : (
          <View />
        )}
        <View style={styles.viewBar}>
          <TouchableOpacity
            onPress={() => handleBarMenu('surah')}
            style={[styles.btnBar, surah && styles.btnBarActive]}>
            <Text style={surah ? styles.textBarActive : styles.textBar}>
              Surah
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.btnBar, juz && styles.btnBarActive]}
            onPress={() => handleBarMenu('juz')}>
            <Text style={juz ? styles.textBarActive : styles.textBar}>Juz</Text>
          </TouchableOpacity>
        </View>
        {surah ? (
          <Surah
            navigation={navigation}
            surahs={
              isEdit && filtered.length > 0
                ? filtered
                : isEdit && filtered.length === 0
                ? []
                : data?.surahs?.references
            }
          />
        ) : (
          <Juz
            navigation={navigation}
            juz={
              isEdit && filtered.length > 0
                ? filtered
                : isEdit && filtered.length === 0
                ? []
                : data?.juzs?.references
            }
            data={
              isEdit && filtered.length > 0
                ? filtered
                : isEdit && filtered.length === 0
                ? []
                : data?.surahs?.references
            }
            isEdit={isEdit}
            filter={filtered}
          />
        )}
      </View>
    </SafeAreaView>
  );
}

export default Home;
