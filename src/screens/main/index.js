import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../home';
import Prayer from '../prayer';
import {PrayActive, PrayInactive, ReadActive, ReadInactive} from '../../assets';

const Tab = createBottomTabNavigator();

function Main() {
  return (
    <Tab.Navigator
      initialRouteName="home"
      screenOptions={{
        tabBarStyle: {
          elevation: 1.5,
          paddingVertical: 15,
          borderTopWidth: 0,
          marginHorizontal: -10,
        },
        headerShown: false,
      }}>
      <Tab.Screen
        name="home"
        component={Home}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({focused}) => {
            return focused ? <ReadActive /> : <ReadInactive />;
          },
        }}
      />
      <Tab.Screen
        name="prayer"
        component={Prayer}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({focused}) => {
            return focused ? <PrayActive /> : <PrayInactive />;
          },
        }}
      />
    </Tab.Navigator>
  );
}

export default Main;
