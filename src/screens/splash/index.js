import React, {useEffect, useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {SafeAreaView, View, Text} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import {useTheme} from '@react-navigation/native';
import {useStyles} from './styles';
import {BgQuranApp} from '../../assets';
import {setPosition} from '../../redux/actions/core';
import {getPlace} from '../../redux/actions/splash';

function Splash({navigation}) {
  const {colors} = useTheme();
  const styles = useStyles(colors);
  const dispatch = useDispatch();

  const {walkthrough, permissionGrant} = useSelector(state => state.core);

  console.log('grant -> ', walkthrough);
  const requestLocation = useCallback(() => {
    if (permissionGrant) {
      Geolocation.getCurrentPosition(
        position => {
          const params = {
            lat: position?.coords?.latitude,
            long: position?.coords?.longitude,
            sensor: true,
            key: 'AIzaSyD5kc1Jcy9TNkz3zLKrk3IvLFaIJ7B45Io',
          };
          dispatch(setPosition(position?.coords));
          dispatch(getPlace(params));
        },
        error => {
          console.log('ERROR');
        },
        {
          enableHighAccuracy: true,
          timeout: 15000,
          maximumAge: 1000,
        },
      );
    }
  }, [dispatch, permissionGrant]);
  useEffect(() => {
    requestLocation();
    setTimeout(() => {
      if (walkthrough) {
        navigation.reset({index: 0, routes: [{name: 'main'}]});
      } else {
        navigation.navigate('walk');
      }
    }, 500);
  }, [navigation, walkthrough, requestLocation]);

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.container}>
        <BgQuranApp />
        <View style={styles.viewVer}>
          <Text style={styles.version}>Version 1</Text>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default Splash;
