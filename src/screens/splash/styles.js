import {StyleSheet} from 'react-native';

export const useStyles = colors =>
  StyleSheet.create({
    safeArea: {
      flex: 1,
      backgroundColor: colors.background,
    },
    container: {
      height: '100%',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    version: {
      fontSize: 16,
      color: colors.textSecondary,
    },
    viewVer: {
      marginTop: 20,
    },
  });
