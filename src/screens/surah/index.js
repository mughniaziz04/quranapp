import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useTheme} from '@react-navigation/native';
import {getDataSurah} from '../../redux/surah';
import {View, Text, FlatList, ActivityIndicator} from 'react-native';
import {SurahItem} from '../../components';
import useStyles from './styles';

function Surah({navigation, surahs}) {
  const dispatch = useDispatch();
  // const {surah} = useSelector(state => state.surah);
  const {colors} = useTheme();
  const styles = useStyles(colors);

  // useEffect(() => {
  //   dispatch(getDataSurah());
  // }, [dispatch]);

  const navigateDetail = id => {
    navigation.navigate('detail', {
      id: id?.item?.number,
      title: id?.item?.englishName,
      transName: id?.item?.englishNameTranslation,
      ayahLength: id?.item?.numberOfAyahs,
      revelation: id?.item?.revelationType,
    });
  };

  return (
    <FlatList
      contentContainerStyle={styles.list}
      initialNumToRender={10}
      maxToRenderPerBatch={10}
      data={surahs}
      renderItem={item => (
        <SurahItem item={item} onPress={() => navigateDetail(item)} />
      )}
      showsVerticalScrollIndicator={false}
    />
  );
}

export default Surah;
