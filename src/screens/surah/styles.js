import {StyleSheet} from 'react-native';

const useStyles = colors =>
  StyleSheet.create({
    list: {
      marginBottom: 220,
      paddingBottom: 220,
    },
    textLoad: {
      fontFamily: 'Poppins-Bold',
      fontSize: 20,
      color: colors.textLatin,
    },
    viewEmpty: {
      marginTop: 150,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

export default useStyles;
