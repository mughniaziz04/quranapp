import React from 'react';
import {useTheme} from '@react-navigation/native';
import {View, Text, ImageBackground, FlatList} from 'react-native';
import {Position} from '../../assets';
import {useStyles} from './styles';

function Juz({navigation, juz, data, isEdit, filter}) {
  const {colors} = useTheme();
  const styles = useStyles(colors);
  const surah = item => {
    return data.find(itm => itm.number === item.surah);
  };
  return (
    <FlatList
      contentContainerStyle={{paddingBottom: 150}}
      data={isEdit ? filter : juz}
      renderItem={({item, index}) => (
        <View style={styles.viewContent}>
          <View style={styles.viewLeft}>
            <ImageBackground source={Position} style={styles.imgBg}>
              <Text style={styles.fontPosition}>{index + 1}</Text>
            </ImageBackground>
            <View style={styles.viewName}>
              <Text style={styles.textName}>
                {isEdit ? item?.englishName : surah(item)?.englishName}
              </Text>
              <View style={styles.viewDesc}>
                <Text style={styles.textDesc}>
                  VERSES {isEdit ? item?.numberOfAyahs : item?.ayah}
                </Text>
              </View>
            </View>
          </View>
          <View>
            <Text style={styles.textArab}>
              {isEdit ? item?.name : surah(item)?.name}
            </Text>
          </View>
        </View>
      )}
      showsVerticalScrollIndicator={false}
    />
  );
}

export default Juz;
