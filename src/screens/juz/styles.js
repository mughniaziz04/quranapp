import {StyleSheet} from 'react-native';

export const useStyles = colors =>
  StyleSheet.create({
    fontPosition: {
      fontFamily: 'Poppins-Regular',
      fontSize: 13,
      color: colors.textLatin,
    },
    viewContent: {
      flexDirection: 'row',
      width: '100%',
      height: 90,
      marginTop: 10,
      alignItems: 'center',
      justifyContent: 'space-between',
      borderBottomColor: colors.border,
      borderBottomWidth: 1,
    },
    imgBg: {
      width: 40,
      height: 40,
      justifyContent: 'center',
      alignItems: 'center',
    },
    viewLeft: {
      paddingVertical: 10,
      flexDirection: 'row',
    },
    viewDesc: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 5,
    },
    viewName: {
      marginLeft: 10,
    },
    dot: {
      height: 3,
      width: 3,
      backgroundColor: colors.border,
      borderRadius: 50,
      marginHorizontal: 3,
      marginTop: 2,
    },
    textArab: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 14,
      color: colors.textPrimary,
    },
    textName: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 14,
      color: colors.textLatin,
    },
    textDesc: {
      fontFamily: 'Poppins-Regular',
      fontSize: 12,
      color: colors.textSecondary,
    },
  });
