import React, {useEffect, useState} from 'react';
import {SafeAreaView, View, Text} from 'react-native';
import {getDataTimming} from '../../redux/prayer';
import {useSelector, useDispatch} from 'react-redux';
import {Header} from '../../components';

function Prayer() {
  const dispatch = useDispatch();
  const {namePlace} = useSelector(state => state.core);
  const {timing} = useSelector(state => state.prayer);

  console.log('NAMEPLACE -> ', namePlace);

  useEffect(() => {
    dispatch(getDataTimming(namePlace));
  }, [dispatch, namePlace]);

  return (
    <SafeAreaView style={{flex: 1}}>
      <Header title="Prayer Time" />
      <View style={{padding: 20}}>
        {/* <View
          style={{
            paddingVertical: 10,
            paddingHorizontal: 10,
            // marginRight: namePlace.length > 20 ? 60 : 20,
          }}>
          <Text>Lokasi</Text>
          <Text>{namePlace}</Text>
        </View> */}
        <View style={{borderWidth: 1, borderRadius: 20}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 20,
            }}>
            <Text>Tanggal Georgian</Text>
            <Text>Tanggal Hijri</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View>
              <View
                style={{
                  paddingHorizontal: 20,
                  flexDirection: 'row',
                  paddingVertical: 10,
                  justifyContent: 'space-between',
                }}>
                <Text>Imsyak: </Text>
                <Text>Jam Imsyak</Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 20,
                  flexDirection: 'row',
                  paddingVertical: 10,
                  justifyContent: 'space-between',
                }}>
                <Text>Subuh: </Text>
                <Text>Jam Subuh</Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 20,
                  flexDirection: 'row',
                  paddingVertical: 10,
                  justifyContent: 'space-between',
                }}>
                <Text>Dzuhur: </Text>
                <Text>Jam Dzuhur</Text>
              </View>
            </View>
            <View>
              <View
                style={{
                  paddingHorizontal: 20,
                  flexDirection: 'row',
                  paddingVertical: 10,
                  justifyContent: 'space-between',
                }}>
                <Text>Ashar: </Text>
                <Text>Jam Ashar</Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 20,
                  flexDirection: 'row',
                  paddingVertical: 10,
                  justifyContent: 'space-between',
                }}>
                <Text>Maghrib: </Text>
                <Text>Jam Maghrib</Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 20,
                  flexDirection: 'row',
                  paddingVertical: 10,
                  justifyContent: 'space-between',
                }}>
                <Text>Isya: </Text>
                <Text>Jam Isya</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default Prayer;
