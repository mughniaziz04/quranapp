import {StyleSheet} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const useStyles = colors =>
  StyleSheet.create({
    safeArea: {
      flex: 1,
      backgroundColor: colors.background,
    },
    container: {
      height: '70%',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    welcomeText: {
      fontSize: 30,
      fontWeight: '700',
      color: colors.textPrimary,
    },
    textApp: {
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 70,
    },
    desc: {
      textAlign: 'center',
      marginTop: 20,
      fontSize: 18,
      color: colors.textSecondary,
    },
    btnStart: {
      backgroundColor: colors.btnSecondary,
      padding: 20,
      // position: 'absolute',
      // bottom: hp(2),
      borderRadius: 50,
      alignItems: 'center',
      justifyContent: 'center',
      width: '60%',
    },
    textBtn: {
      color: colors.textWhite,
      fontSize: 22,
      fontWeight: '700',
    },
  });
