import React from 'react';
import {SafeAreaView, View, Text, TouchableOpacity} from 'react-native';
import {useDispatch} from 'react-redux';
import {useTheme} from '@react-navigation/native';
import {useStyles} from './styles';
import {BgQuranApp} from '../../assets';
import {setWalkthrough} from '../../redux/actions/core';

function Walkthrough({navigation}) {
  const {colors} = useTheme();
  const styles = useStyles(colors);

  const dispatch = useDispatch();

  const goToHome = () => {
    dispatch(setWalkthrough(true));
    navigation.reset({index: 0, routes: [{name: 'main'}]});
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <View style={styles.textApp}>
        <Text style={styles.welcomeText}>Quran App</Text>
        <Text style={styles.desc}>
          Learn Quran and{'\n'}Recite once everyday
        </Text>
      </View>
      <View style={styles.container}>
        <BgQuranApp />
        <TouchableOpacity style={styles.btnStart} onPress={() => goToHome()}>
          <Text style={styles.textBtn}>Get Started</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

export default Walkthrough;
