import {StyleSheet} from 'react-native';

export const useStyles = colors =>
  StyleSheet.create({
    safeArea: {
      flex: 1,
      backgroundColor: colors.background,
    },
    container: {
      padding: 20,
    },
    textSurahName: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 25,
      color: colors.textWhite,
    },
    imgBg: {
      height: 257,
      alignItems: 'center',
      paddingVertical: 30,
    },
    viewHead: {
      alignItems: 'center',
      borderBottomWidth: 1,
      borderBottomColor: colors.border,
      width: '80%',
      height: '50%',
    },
    textSurahTranslate: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 16,
      color: colors.textWhite,
    },
    viewDetailSurah: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 15,
    },
    textRevelation: {
      fontFamily: 'Poppins-Regular',
      fontSize: 16,
      color: colors.textWhite,
    },
    dot: {
      height: 4,
      width: 4,
      borderRadius: 50,
      backgroundColor: colors.border,
      marginHorizontal: 5,
    },
    viewAyat: {
      marginTop: 30,
    },
    viewHeadAyat: {
      width: '100%',
      height: 35,
      backgroundColor: colors.cardHeader,
      borderRadius: 10,
      justifyContent: 'center',
      paddingHorizontal: 10,
    },
    viewRoundAyat: {
      height: 25,
      width: 25,
      borderRadius: 50,
      backgroundColor: colors.roundAyat,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textNumberAyat: {
      color: colors.textWhite,
      fontFamily: 'Poppins-Regular',
      fontSize: 10,
    },
    textAyat: {
      fontFamily: 'Poppins-SemiBold',
      fontSize: 20,
      color: colors.textLatin,
    },
    textTransalate: {
      fontFamily: 'Poppins-Regular',
      fontSize: 16,
      color: colors.textLatin,
    },
    list: {
      paddingBottom: 300,
    },
    viewText: {
      marginTop: 10,
    },
    viewItems: {
      marginTop: 20,
    },
    viewListItem: {
      marginVertical: 10,
    },
    viewLoad: {
      justifyContent: 'center',
      alignItems: 'center',
      height: '100%',
    },
    textLoad: {
      fontFamily: 'Poppins-Bold',
      fontSize: 20,
      color: colors.textLatin,
    },
  });
