import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useTheme} from '@react-navigation/native';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import {Header} from '../../components';
import {BgSurah} from '../../assets';
import {getDetailSurah, resetState} from '../../redux/actions/detail';
import {useStyles} from './styles';

function DetailSurah({navigation, route}) {
  const dispatch = useDispatch();
  const {arab, trans, isLoad, offset, limit} = useSelector(
    state => state.detail,
  );
  const {ayahs} = arab;
  const {colors} = useTheme();
  const styles = useStyles(colors);

  const id = route?.params?.id;

  useEffect(() => {
    return () => {
      dispatch(resetState());
    };
  }, [dispatch]);

  useEffect(() => {
    dispatch(getDetailSurah(id, offset, limit));
  }, [dispatch, id, offset, limit]);

  const renderItem = (item, index) => {
    return (
      <View style={styles.viewListItem} key={index}>
        <View style={styles.viewHeadAyat}>
          <View style={styles.viewRoundAyat}>
            <Text style={styles.textNumberAyat}>{item?.numberInSurah}</Text>
          </View>
        </View>
        <View style={styles.viewText}>
          <Text style={styles.textAyat}>{item?.text}</Text>
          <Text style={styles.textTransalate}>
            {trans?.ayahs?.find(tr => tr.number === item.number)?.text}
          </Text>
        </View>
      </View>
    );
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const getData = ({nativeEvent}) => {
    if (isCloseToBottom(nativeEvent) && route?.params?.ayahLength > limit) {
      // dispatch(getDetailSurahSuccess([]));
      // dispatch(updateDataOffset(0));
    }
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <Header
        title={route.params.title}
        canBack={navigation.canGoBack()}
        onPress={() => navigation.goBack()}
      />
      {isLoad ? (
        <View style={styles.viewLoad}>
          <ActivityIndicator size="large" />
          <Text style={styles.textLoad}>Loading...</Text>
        </View>
      ) : (
        <ScrollView onScroll={({nativeEvent}) => getData({nativeEvent})}>
          <View style={styles.container}>
            <ImageBackground
              source={BgSurah}
              borderRadius={25}
              style={styles.imgBg}>
              <View style={styles.viewHead}>
                <Text style={styles.textSurahName}>{route?.params?.title}</Text>
                <Text style={styles.textSurahTranslate}>
                  {route?.params?.transName}
                </Text>
              </View>
              <View style={styles.viewDetailSurah}>
                <Text style={styles.textRevelation}>
                  {route?.params?.revelation?.toUpperCase()}
                </Text>
                <View style={styles.dot} />
                <Text style={styles.textRevelation}>
                  {route?.params?.ayahLength} VERSES
                </Text>
              </View>
            </ImageBackground>
            <View style={styles.viewItems}>
              {ayahs?.map((item, index) => renderItem(item, index))}
            </View>
          </View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
}

export default DetailSurah;
